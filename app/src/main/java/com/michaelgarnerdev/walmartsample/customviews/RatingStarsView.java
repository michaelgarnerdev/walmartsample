package com.michaelgarnerdev.walmartsample.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.michaelgarnerdev.walmartsample.BuildConfig;
import com.michaelgarnerdev.walmartsample.R;

/**
 * Created by mgarner on 4/11/2016.
 * This custom view helps display ratings in a uniform fashion across all versions of Android.
 */
public class RatingStarsView extends FrameLayout {
    private static final double TOTAL_POSSIBLE_RATING = 5.0d;
    private View mVBackgroundFill;
    private View mParentView;

    public RatingStarsView(Context context) {
        super(context);
        initView(context);
    }

    public RatingStarsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public RatingStarsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    /**
     * Sets the rating that should be displayed. This will cause the view to shade in the appropriate number of stars.
     * @param rating The rating as a double.
     */
    public void setRating(double rating) {
        double ratingPercentage = rating / TOTAL_POSSIBLE_RATING;
        try {
            setBackgroundFillPercentage(ratingPercentage);
        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Sets the background fill width proportional to the rating percentage passed in.
     * @param ratingPercentage The percentage value of the rating.
     * @throws Exception If a parent LayoutParams can't be found, an exception is thrown. Acceptable parent layouts include LinearLayout, RelativeLayout, and FrameLayout.
     */
    private void setBackgroundFillPercentage(double ratingPercentage) throws Exception {
        if (mVBackgroundFill != null) {
            FrameLayout.LayoutParams layoutParams = (LayoutParams) mVBackgroundFill.getLayoutParams();
            if (mParentView.getLayoutParams() instanceof FrameLayout.LayoutParams) {
                FrameLayout.LayoutParams parentLayoutParams = (FrameLayout.LayoutParams) mParentView.getLayoutParams();
                layoutParams.width = (int) (parentLayoutParams.width * ratingPercentage);
            } else if (mParentView.getLayoutParams() instanceof LinearLayout.LayoutParams) {
                LinearLayout.LayoutParams parentLayoutParams = (LinearLayout.LayoutParams) mParentView.getLayoutParams();
                layoutParams.width = (int) (parentLayoutParams.width * ratingPercentage);
            } else if (mParentView.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
                RelativeLayout.LayoutParams parentLayoutParams = (RelativeLayout.LayoutParams) mParentView.getLayoutParams();
                layoutParams.width = (int) (parentLayoutParams.width * ratingPercentage);
            } else {
                throw new Exception("Invalid parent layout.");
            }
            mVBackgroundFill.setLayoutParams(layoutParams);
        }
    }

    /**
     * Sets up the custom views within this custom view that make the magic happen.
     * @param context Need a context to inflate. Duh.
     */
    public void initView(Context context) {
        mParentView = inflate(context, R.layout.view_rating_stars, this);
        mVBackgroundFill = mParentView.findViewById(R.id.rating_stars_view_v_background_fill);
    }
}
