package com.michaelgarnerdev.walmartsample.activities;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.transition.Slide;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.michaelgarnerdev.walmartsample.R;
import com.michaelgarnerdev.walmartsample.customviews.RatingStarsView;
import com.michaelgarnerdev.walmartsample.customviews.SquareImageView;
import com.michaelgarnerdev.walmartsample.objects.Product;
import com.michaelgarnerdev.walmartsample.utils.ProductUtils;
import com.michaelgarnerdev.walmartsample.utils.Utils;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.ArrayList;

@SuppressWarnings("FieldCanBeLocal")
public class ProductDetailActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {

    public static final String EXTRA_PRODUCTS = "extraProducts";
    public static final String EXTRA_POSITION_CURRENT_PRODUCT = "extraPositionCurrentProduct";

    private LinearLayout mParentLayout;
    private TextView mTvName;
    private ArrayList<Product> mProductsList;
    private Product mCurrentProduct;
    private RatingStarsView mRatingStarsView;
    private TextView mTvNumRatings;
    private SquareImageView mSivProductImage;
    private TextView mTvPrice;
    private TextView mTvStockStatus;
    private Button mBtnAddToCart;
    private TextView mTvShortDescription;
    private TextView mTvLongDescription;
    private int mPositionCurrentProduct = -1;
    private int mOriginalProductPosition = -1;
    private ImageButton mBtnShare;
    private ScrollView mScrollView;

    private float prevX;
    private float prevY;
    private static final int MIN_SWIPE_DISTANCE = Utils.dpToPx(100);
    private static final int MAX_VERTICAL_SWIPE_DISTANCE = Utils.dpToPx(50);
    private Handler mSwipeHandler = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            getWindow().setEnterTransition(new Slide(Gravity.END));
            getWindow().setExitTransition(new Slide(Gravity.END));
        }
        setContentView(R.layout.activity_product_detail);
        setupView();
        handleIntent();
    }

    @SuppressWarnings("ConstantConditions")
    private void setupView() {
        setupToolbar();
        mScrollView = (ScrollView) findViewById(R.id.product_detail_scroll_view);
        mScrollView.setOnTouchListener(this);
        mParentLayout = (LinearLayout) findViewById(R.id.product_detail_ll_parent_layout);
        mTvName = (TextView) findViewById(R.id.product_detail_tv_name);
        mRatingStarsView = (RatingStarsView) findViewById(R.id.product_detail_rsv_rating);
        mTvNumRatings = (TextView) findViewById(R.id.product_detail_tv_number_ratings);
        mSivProductImage = (SquareImageView) findViewById(R.id.product_detail_siv_image);
        mTvPrice = (TextView) findViewById(R.id.product_list_item_tv_price);
        mTvStockStatus = (TextView) findViewById(R.id.product_list_item_tv_stock_status);
        mBtnShare = (ImageButton) findViewById(R.id.product_detail_btn_share);
        mBtnAddToCart = (Button) findViewById(R.id.product_detail_btn_add_to_cart);
        mTvShortDescription = (TextView) findViewById(R.id.product_detail_tv_short_description);
        mTvLongDescription = (TextView) findViewById(R.id.product_detail_tv_long_description);
        mBtnAddToCart.setOnClickListener(this);
        mBtnShare.setOnClickListener(this);
        mParentLayout.setOnTouchListener(this);
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.product_detail_toolbar);
        if (toolbar != null) {
            toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            toolbar.setTitle(getString(R.string.app_name));
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white);
            setSupportActionBar(toolbar);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    /**
     * Handles the incoming data from the ProductListActivity
     */
    private void handleIntent() {
        if (getIntent() != null && getIntent().getExtras() != null) {
            mProductsList = new ArrayList<>();
            mProductsList = getIntent().getParcelableArrayListExtra(EXTRA_PRODUCTS);
            mPositionCurrentProduct = getIntent().getIntExtra(EXTRA_POSITION_CURRENT_PRODUCT, -1);
            if (mOriginalProductPosition == -1) {
                mOriginalProductPosition = mPositionCurrentProduct;
            }
            if (mProductsList != null && mPositionCurrentProduct >= 0 && mPositionCurrentProduct < mProductsList.size()) {
                mCurrentProduct = mProductsList.get(mPositionCurrentProduct);
                bindView();
            } else {
                Toast.makeText(this, "Sorry, couldn't find product.", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    /**
     * Binds the product data to the view and then fades the whole view in.
     */
    private void bindView() {
        mParentLayout.setVisibility(View.INVISIBLE);
        mTvName.setText(ProductUtils.normalizeText(mCurrentProduct.getName()));
        if (mCurrentProduct.getNumRatings() > 0) {
            mRatingStarsView.setRating(mCurrentProduct.getRating());
            String totalRatings = getResources().getQuantityString(R.plurals.product_detail_ratings, mCurrentProduct.getNumRatings(), mCurrentProduct.getNumRatings());
            DecimalFormat decimalFormat = new DecimalFormat("0.0");
            totalRatings = decimalFormat.format(mCurrentProduct.getRating()) + " stars with " + totalRatings;
            mTvNumRatings.setText(totalRatings);
        } else {
            mRatingStarsView.setRating(mCurrentProduct.getRating());
            mTvNumRatings.setText(getString(R.string.product_detail_first_to_review));
        }
        if (!TextUtils.isEmpty(mCurrentProduct.getImageUrl())) {
            mSivProductImage.setVisibility(View.VISIBLE);
            Glide.with(this).load(mCurrentProduct.getImageUrl()).into(mSivProductImage);
        } else {
            mSivProductImage.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(mCurrentProduct.getPrice())) {
            mTvPrice.setText(mCurrentProduct.getPrice());
        } else {
            mTvPrice.setText(getString(R.string.product_detail_price_in_cart));
        }
        if (!mCurrentProduct.getStockStatus()) {
            mTvStockStatus.setText(ProductUtils.getStockStatusText(this, false));
            mTvStockStatus.setTextColor(ProductUtils.getStockStatusColor(this, false));
        }
        if (!TextUtils.isEmpty(mCurrentProduct.getShortDescription())) {
            mTvShortDescription.setVisibility(View.VISIBLE);
            mTvShortDescription.setText(ProductUtils.handleHtml(ProductUtils.normalizeText(mCurrentProduct.getShortDescription())));
        } else {
            mTvShortDescription.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(mCurrentProduct.getLongDescription())) {
            mTvLongDescription.setVisibility(View.VISIBLE);
            mTvLongDescription.setText(ProductUtils.handleHtml(ProductUtils.normalizeText(mCurrentProduct.getLongDescription())));
        } else {
            mTvLongDescription.setVisibility(View.GONE);
        }
        mBtnAddToCart.setText(getString(R.string.product_detail_add_to_cart));
        Utils.fadeInView(new WeakReference<View>(mParentLayout));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.product_detail_btn_add_to_cart:
                mBtnAddToCart.setText(getString(R.string.product_detail_added_to_cart));
                break;
            case R.id.product_detail_btn_share:
                ProductUtils.shareProduct(ProductDetailActivity.this, mCurrentProduct);
                break;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                prevX = event.getX();
                prevY = event.getY();
                return false;
            case MotionEvent.ACTION_UP:
                float currentX = event.getX();
                float currentY = event.getY();
                float deltaX;
                if (currentX > prevX) {
                    deltaX = currentX - prevX;
                    if (Math.abs(deltaX) > MIN_SWIPE_DISTANCE && getDeltaY(currentY) < MAX_VERTICAL_SWIPE_DISTANCE) {
                        //swipe right
                        showPreviousProduct();
                    }
                } else {
                    deltaX = prevX - currentX;
                    if (Math.abs(deltaX) > MIN_SWIPE_DISTANCE && getDeltaY(currentY) < MAX_VERTICAL_SWIPE_DISTANCE) {
                        //swipe left
                        showNextProduct();
                    }
                }
                return false;
        }
        return false;
    }

    /**
     * Get the change in Y between touch down and touch up. Takes in the up Y.
     * @param currentY The Y position of the touch UP event.
     * @return deltaY
     */
    private float getDeltaY(float currentY) {
        if (currentY > prevY) {
            return currentY - prevY;
        } else {
            return prevY - currentY;
        }
    }

    /**
     * Shows the previous product (if it exists) from the list activity
     */
    private void showPreviousProduct() {
        if (mPositionCurrentProduct > 0) {
            mPositionCurrentProduct--;
            mCurrentProduct = mProductsList.get(mPositionCurrentProduct);
            if (mParentLayout != null) {
                Utils.fadeOutView(new WeakReference<View>(mParentLayout));
            }
            mSwipeHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    bindView();
                }
            }, 300);
        }
    }

    /**
     * Shows the next product (if it exists) from the list activity
     */
    private void showNextProduct() {
        if (mPositionCurrentProduct < mProductsList.size() - 1) {
            mPositionCurrentProduct++;
            mCurrentProduct = mProductsList.get(mPositionCurrentProduct);
            if (mParentLayout != null) {
                Utils.fadeOutView(new WeakReference<View>(mParentLayout));
            }
            mSwipeHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    bindView();
                }
            }, 300);
        }
    }
}
