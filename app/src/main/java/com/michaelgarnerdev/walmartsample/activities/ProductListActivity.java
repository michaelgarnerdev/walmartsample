package com.michaelgarnerdev.walmartsample.activities;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Slide;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.widget.RelativeLayout;

import com.michaelgarnerdev.walmartsample.adapters.ProductListAdapter;
import com.michaelgarnerdev.walmartsample.R;
import com.michaelgarnerdev.walmartsample.utils.ConnectionUtils;
import com.michaelgarnerdev.walmartsample.utils.Utils;

@SuppressWarnings("unused")
public class ProductListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, ProductListAdapter.OnLoadListener {

    private ProductListAdapter mProductListAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RelativeLayout mRlDimmingOverlay;
    private AlphaAnimation mDimmingOverlayFadeOutAnim;
    private AlphaAnimation mDimmingOverlayFadeInAnim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            getWindow().setEnterTransition(new Slide(Gravity.START));
            getWindow().setExitTransition(new Slide(Gravity.START));
        }
        setContentView(R.layout.activity_product_list);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.app_name));
        }
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.product_list_swipe_refresh_layout);
        assert mSwipeRefreshLayout != null;
        mSwipeRefreshLayout.setOnRefreshListener(this);
        RecyclerView rvProductList = (RecyclerView) findViewById(R.id.product_list_rv_list);
        mProductListAdapter = new ProductListAdapter(this);
        mProductListAdapter.setLoadListener(this);

        mRlDimmingOverlay = (RelativeLayout) findViewById(R.id.product_list_rl_dimming_loading_overlay);
        mDimmingOverlayFadeOutAnim = Utils.setupFadeAnimation(300, 1.0f, 0.0f, mRlDimmingOverlay);
        mDimmingOverlayFadeInAnim = Utils.setupFadeAnimation(300, 0.0f, 1.0f, mRlDimmingOverlay);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        assert rvProductList != null;
        rvProductList.setHasFixedSize(true);
        rvProductList.setLayoutManager(layoutManager);
        rvProductList.setAdapter(mProductListAdapter);
        mSwipeRefreshLayout.setRefreshing(true);
        if (ConnectionUtils.hasInternet(this)) {
            mProductListAdapter.getFirstProducts();
        } else {
            Snackbar.make(mSwipeRefreshLayout, "Sorry, you don't have internet right now. Please connect and try again.", Snackbar.LENGTH_LONG).setAction("Try Again", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mProductListAdapter != null) {
                        mProductListAdapter.getFirstProducts();
                    }
                }
            }).show();
        }
    }

    /**
     * Show progress bar
     */
    private void showDimmingOverlay() {
        if (mRlDimmingOverlay != null && mDimmingOverlayFadeInAnim != null) {
            mRlDimmingOverlay.startAnimation(mDimmingOverlayFadeInAnim);
        }
    }

    /**
     * Hide progress bar
     */
    private void hideDimmingOverlay() {
        if (mRlDimmingOverlay != null && mDimmingOverlayFadeOutAnim != null) {
            mRlDimmingOverlay.startAnimation(mDimmingOverlayFadeOutAnim);
        }
    }

    /**
     * Called when the user swipes down at the top.
     */
    @Override
    public void onRefresh() {
        if (mProductListAdapter != null) {
            mProductListAdapter.clearAdapter();
            mProductListAdapter.getFirstProducts();
        }
    }

    @Override
    public void onFirstItemsLoaded(int numLoaded) {
        hideDimmingOverlay();
    }

    @Override
    public void onNextItemsLoaded(int numLoaded) {

    }

    @Override
    public void onFirstItemsFailure() {
        Snackbar.make(mSwipeRefreshLayout, "Sorry, we were unable to load any products at the moment. Please try again later.", Snackbar.LENGTH_LONG).setAction("Try Again", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mProductListAdapter != null) {
                    mProductListAdapter.getFirstProducts();
                }
            }
        }).show();
    }

    @Override
    public void onNextItemsFailure() {
        Snackbar.make(mSwipeRefreshLayout, "Sorry, we were unable to load more products at the moment. Please try again later.", Snackbar.LENGTH_LONG).setAction("Try Again", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mProductListAdapter != null) {
                    mProductListAdapter.getNextPage();
                }
            }
        }).show();
    }

    @Override
    public void onLoadStarted() {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(true);
        }
    }

    @Override
    public void onLoadFinished() {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    /**
     * Clean up custom interfaces to prevent possible memory leaks.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mProductListAdapter != null) {
            mProductListAdapter.setLoadListener(null);
            mProductListAdapter.clearAdapter();
            mProductListAdapter.destroyAdapter();
        }
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setOnRefreshListener(null);
        }
    }
}
