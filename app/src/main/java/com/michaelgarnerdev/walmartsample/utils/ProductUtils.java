package com.michaelgarnerdev.walmartsample.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;

import com.google.gson.Gson;
import com.michaelgarnerdev.walmartsample.BuildConfig;
import com.michaelgarnerdev.walmartsample.objects.Product;
import com.michaelgarnerdev.walmartsample.objects.ProductResponse;
import com.michaelgarnerdev.walmartsample.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.XMLReader;

import java.io.IOException;
import java.text.Normalizer;
import java.util.ArrayList;

import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by mgarner on 4/11/2016.
 * This Utils class helps perform actions on products and their data.
 */
@SuppressWarnings("unused")
public class ProductUtils {

    public static final int DEFAULT_PAGE_SIZE = 25;

    private static final String KEY_STATUS = "status";
    private static final String KEY_PRODUCTS = "products";
    private static final String KEY_NUMBER_PRODUCTS = "totalProducts";
    private static final String KEY_PAGE_NUMBER = "pageNumber";
    private static final String KEY_PAGE_SIZE = "pageSize";

    /**
     * Returns a string based on the input stockStatus.
     * @param context I'll need that.
     * @param stockStatus true if the product is in stock.
     * @return String indicating status.
     */
    public static String getStockStatusText(Context context, boolean stockStatus) {
        if (context != null) {
            if (stockStatus) {
                return context.getString(R.string.product_in_stock);
            } else {
                return context.getString(R.string.product_out_of_stock);
            }
        } else {
            return null;
        }
    }

    /**
     * Formulates query to API with default page size.
     * @param callback How shall I respond to said query?
     * @return Callback
     */
    public static Callback getInitialProducts(Callback callback) {
        return getInitialProducts(callback, DEFAULT_PAGE_SIZE);
    }

    /**
     * Formulates query to API with input page size.
     * @param callback How shall I respond to said query?
     * @param numProducts How many products should we try to get?
     * @return Callback
     */
    public static Callback getInitialProducts(Callback callback, final int numProducts) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(getProductsApiUrl(-1, numProducts))
                .build();
        client.newCall(request).enqueue(callback);
        return callback;
    }

    /**
     * Formulates query to API with input page number and default page size.
     * @param callback How shall I respond to said query?
     * @param pageNumber What page are we looking for?
     * @return Callback
     */
    public static Callback getNextPageProducts(Callback callback, int pageNumber) {
        return getNextPageProducts(callback, pageNumber, DEFAULT_PAGE_SIZE);
    }

    /**
     * Formulates query to API with input page number and size.
     * @param callback How shall I respond to said query?
     * @param pageNumber What page are we looking for?
     * @param numProducts How many products should we try to get?
     * @return Callback
     */
    public static Callback getNextPageProducts(Callback callback, int pageNumber, final int numProducts) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(getProductsApiUrl(pageNumber, numProducts))
                .build();
        client.newCall(request).enqueue(callback);
        return callback;
    }

    /**
     * Build and return the appropriate API URL.
     * @param pageNumber The page number desired.
     * @param numProducts The number of products desired.
     * @return String URL
     */
    @SuppressWarnings("StringBufferReplaceableByString")
    //String Builder is more efficient for string concatentation in Java. Use it.
    private static String getProductsApiUrl(int pageNumber, int numProducts) {
        String separator = "/";
        StringBuilder urlBuilder = new StringBuilder();
        urlBuilder.append(BuildConfig.API_HITPOINT);
        urlBuilder.append(BuildConfig.API_KEY);
        urlBuilder.append(separator);
        if (pageNumber == -1) {
            urlBuilder.append(1);
        } else {
            urlBuilder.append(pageNumber);
        }
        urlBuilder.append(separator);
        urlBuilder.append(numProducts);
        if (BuildConfig.DEBUG) {
            Log.d(ProductUtils.class.getSimpleName(), "PRODUCTS API URL: " + urlBuilder.toString());
        }
        return urlBuilder.toString();
    }

    /**
     * Takes the response from a query to the Products API and parses it into Objects.
     * @param response The response from the API
     * @param numProducts The number of products retrieved. Used to initialize the products list to a default size large enough to contain the data.
     * @return ProductResponse
     */
    public static ProductResponse handleProductResponse(Response response, int numProducts) {
        ProductResponse productResponse = new ProductResponse();
        ArrayList<Product> products = new ArrayList<>(numProducts);
        int totalProducts = 0;
        if (response != null && response.isSuccessful()) {
            JSONObject responseJson;
            try {
                responseJson = new JSONObject(response.body().string());
                JSONArray productsArray = responseJson.optJSONArray(KEY_PRODUCTS);
                totalProducts = responseJson.optInt(KEY_NUMBER_PRODUCTS);
                if (productsArray != null && productsArray.length() > 0) {
                    Gson gson = new Gson();
                    for (int i = 0; i < productsArray.length(); i++) {
                        Product product = gson.fromJson(productsArray.getJSONObject(i).toString(), Product.class);
                        products.add(product);
                    }
                }
            } catch (JSONException | IOException e) {
                e.printStackTrace();
            }
        }
        productResponse.setProducts(products);
        productResponse.setTotalProducts(totalProducts);
        return productResponse;
    }

    /**
     * Get the number of products in the response.
     * @param response The response from the API.
     * @return int number of products.
     */
    public static int getTotalProducts(Response response) {
        if (response != null && response.isSuccessful()) {
            JSONObject responseJson;
            try {
                responseJson = new JSONObject(response.body().string());
                return responseJson.optInt(KEY_NUMBER_PRODUCTS);
            } catch (JSONException | IOException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    /**
     * Gets the color value based on input status.
     * @param context I'll need this.
     * @param stockStatus true if the product is in stock.
     * @return int representation of color.
     */
    public static int getStockStatusColor(Context context, boolean stockStatus) {
        if (stockStatus) {
            return ContextCompat.getColor(context, R.color.in_stock_green);
        } else {
            return ContextCompat.getColor(context, R.color.out_of_stock_red);
        }
    }

    /**
     * Helps remove the pesky weird and strange non-ASCII characters.
     * @param string The string to normalize.
     * @return Normalized String.
     */
    public static String normalizeText(String string) {
        return Normalizer.normalize(string, Normalizer.Form.NFD).replaceAll("[^\\x00-\\x7F]", " ");
    }

    /**
     * Helps decipher HTML that is contained in some of the product data.
     * @param html Input String to decipher.
     * @return Spanned object with beautiful HTML representations.
     */
    public static Spanned handleHtml(String html) {
        if (BuildConfig.DEBUG) {
            Log.d(ProductUtils.class.getSimpleName(), html);
        }
        Html.TagHandler tagHandler = new Html.TagHandler() {
            @Override
            public void handleTag(boolean opening, String tag, Editable output, XMLReader xmlReader) {
                if (BuildConfig.DEBUG) {
                    Log.d(ProductUtils.class.getSimpleName(), tag);
                }
                if (tag.equals("li") && opening) {
                    output.append(Html.fromHtml("<br>&nbsp;&#8226;&nbsp;"));
                } else {
                    output.append("");
                }
            }
        };
        return Html.fromHtml(html, null, tagHandler);
    }

    /**
     * Launches chooser for where to share the product's information. Would need a product URL for this to be truly helpful.
     * @param activity Where are you calling from?
     * @param currentProduct What product are you sharing?
     */
    @SuppressWarnings("StringBufferReplaceableByString")
    public static void shareProduct(Activity activity, Product currentProduct) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/html");
        StringBuilder shareTextBuilder = new StringBuilder();
        shareTextBuilder.append("I found this product from Walmart:\n");
        shareTextBuilder.append(currentProduct.getName());
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareTextBuilder.toString());
        activity.startActivity(Intent.createChooser(sharingIntent, "Share using"));
    }
}
