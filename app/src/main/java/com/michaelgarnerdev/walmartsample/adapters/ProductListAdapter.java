package com.michaelgarnerdev.walmartsample.adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.michaelgarnerdev.walmartsample.BuildConfig;
import com.michaelgarnerdev.walmartsample.R;
import com.michaelgarnerdev.walmartsample.customviews.RatingStarsView;
import com.michaelgarnerdev.walmartsample.activities.ProductDetailActivity;
import com.michaelgarnerdev.walmartsample.activities.ProductListActivity;
import com.michaelgarnerdev.walmartsample.objects.Product;
import com.michaelgarnerdev.walmartsample.objects.ProductResponse;
import com.michaelgarnerdev.walmartsample.utils.ProductUtils;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.Response;

/**
 * Created by mgarner on 4/11/2016.
 * This adapter contains the queries to retrieve product data and handles binding the data to the recycler view's view holder.
 */
@SuppressWarnings("unused")
public class ProductListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Product> mProductList = new ArrayList<>(25);
    private WeakReference<Activity> mActivity;
    private int mCurrentPage = 1;
    private static final int LOAD_MORE_THRESHOLD = 9;
    private boolean mIsLoadingMore = false;
    private ProductListActivity mLoadListener;
    private boolean mHasMoreProducts = true;
    private int mTotalProducts = 0;

    public ProductListAdapter(Activity activity) {
        mActivity = new WeakReference<>(activity);
    }

    public ProductListAdapter(Activity activity, ArrayList<Product> products) {
        mActivity = new WeakReference<>(activity);
        mProductList = products;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(mActivity.get());
        View view = inflater.inflate(R.layout.list_item_product, parent, false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ProductViewHolder) {
            bindProductViewHolder((ProductViewHolder) holder, position);
        }
        if (position + LOAD_MORE_THRESHOLD >= getItemCount() && !mIsLoadingMore) {
            getNextPage();
        }
    }

    private void bindProductViewHolder(ProductViewHolder holder, final int position) {
        final Product currentProduct = mProductList.get(position);
        if (mActivity != null && !TextUtils.isEmpty(currentProduct.getImageUrl())) {
            Glide.with(mActivity.get()).load(currentProduct.getImageUrl()).into(holder.mIvProductImage);
        }
        holder.mParentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToDetailViewActivity(position);
            }
        });

        holder.mTvName.setText(ProductUtils.normalizeText(currentProduct.getName()));
        holder.mTvPrice.setText(currentProduct.getPrice());
        holder.mTvStockStatus.setText(ProductUtils.getStockStatusText(mActivity.get(), currentProduct.getStockStatus()));
        holder.mTvStockStatus.setTextColor(ProductUtils.getStockStatusColor(mActivity.get(), currentProduct.getStockStatus()));
        if (currentProduct.getNumRatings() > 0) {
            holder.mLlRatingSection.setVisibility(View.VISIBLE);
            holder.mRsvRating.setRating(currentProduct.getRating());
            holder.mTvNumRatings.setText(String.format(mActivity.get().getString(R.string.product_num_ratings), currentProduct.getNumRatings()));
        } else {
            holder.mLlRatingSection.setVisibility(View.GONE);
        }

    }

    /**
     * Navigates to the ProductDetailActivity with the current product list and the position selected.
     * @param position The position of the product within the product list.
     */
    @SuppressWarnings("unchecked")
    private void goToDetailViewActivity(int position) {
        Intent viewDetailIntent = new Intent(mActivity.get(), ProductDetailActivity.class);
        viewDetailIntent.putParcelableArrayListExtra(ProductDetailActivity.EXTRA_PRODUCTS, mProductList);
        viewDetailIntent.putExtra(ProductDetailActivity.EXTRA_POSITION_CURRENT_PRODUCT, position);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mActivity.get().startActivity(viewDetailIntent, ActivityOptions.makeSceneTransitionAnimation(mActivity.get()).toBundle());
        } else {
            mActivity.get().startActivity(viewDetailIntent);
        }
    }

    @Override
    public int getItemCount() {
        return mProductList != null ? mProductList.size() : 0;
    }

    /**
     * Ensure reference is destroyed.
     */
    public void destroyAdapter() {
        mActivity = null;
    }


    public void setProductList(ArrayList<Product> productList) {
        mProductList = productList;
        notifyDataSetChanged();
    }

    /**
     * Retrieve the first page of results from the products API.
     */
    public void getFirstProducts() {
        Callback callback = new Callback() {
            Handler mainHandler = new Handler(mActivity.get().getMainLooper());

            @Override
            public void onFailure(Call call, final IOException e) {
                mainHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mIsLoadingMore = false;
                        if (BuildConfig.DEBUG) {
                            e.printStackTrace();
                        }
                        if (mLoadListener != null) {
                            mLoadListener.onFirstItemsFailure();
                            mLoadListener.onLoadFinished();
                        }
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                mainHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            mIsLoadingMore = false;
                            if (!response.isSuccessful()) {
                                if (mLoadListener != null) {
                                    mLoadListener.onFirstItemsFailure();
                                    mLoadListener.onLoadFinished();
                                }
                                throw new IOException("Unexpected code " + response);
                            }
                            //mTotalProducts = ProductUtils.getTotalProducts(response);
                            ProductResponse productResponse = ProductUtils.handleProductResponse(response, ProductUtils.DEFAULT_PAGE_SIZE);
                            mTotalProducts = productResponse.getTotalProducts();
                            setProductList(productResponse.getProducts());
                            if (mProductList.size() >= mTotalProducts) {
                                mHasMoreProducts = false;
                            }
                            if (mLoadListener != null) {
                                mLoadListener.onFirstItemsLoaded(mProductList.size());
                                mLoadListener.onLoadFinished();
                            }
                            if (BuildConfig.DEBUG) {
                                Headers responseHeaders = response.headers();
                                for (int i = 0, size = responseHeaders.size(); i < size; i++) {
                                    Log.d(ProductListAdapter.class.getSimpleName(), responseHeaders.name(i) + ": " + responseHeaders.value(i));
                                }
                                Log.d(ProductListAdapter.class.getSimpleName(), response.body().string());
                            }
                        } catch (IOException e) {
                            if (BuildConfig.DEBUG) {
                                e.printStackTrace();
                            }
                        }
                    }
                });

            }
        };
        if (!mIsLoadingMore) {
            mIsLoadingMore = true;
            if (mLoadListener != null) {
                mLoadListener.onLoadStarted();
            }
            ProductUtils.getInitialProducts(callback);
        }
    }

    /**
     * Retrieve the next page of results from the products API.
     */
    public void getNextPage() {
        Callback callback = new Callback() {
            Handler mainHandler = new Handler(mActivity.get().getMainLooper());

            @Override
            public void onFailure(Call call, final IOException e) {
                if (mainHandler != null) {
                    mainHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            mIsLoadingMore = false;
                            if (BuildConfig.DEBUG) {
                                e.printStackTrace();
                            }
                            if (mLoadListener != null) {
                                mLoadListener.onNextItemsFailure();
                                mLoadListener.onLoadFinished();
                            }
                        }
                    });
                }

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (mainHandler != null) {
                    mainHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                mIsLoadingMore = false;
                                if (!response.isSuccessful()) {
                                    if (mLoadListener != null) {
                                        mLoadListener.onNextItemsFailure();
                                        mLoadListener.onLoadFinished();
                                    }
                                    throw new IOException("Unexpected code " + response);
                                }
                                mCurrentPage++;
                                ProductResponse productResponse = ProductUtils.handleProductResponse(response, ProductUtils.DEFAULT_PAGE_SIZE);
                                mTotalProducts = productResponse.getTotalProducts();
                                addToProductList(productResponse.getProducts());
                                if (mProductList.size() >= mTotalProducts) {
                                    mHasMoreProducts = false;
                                }
                                if (mLoadListener != null) {
                                    mLoadListener.onNextItemsLoaded(productResponse.getProducts().size());
                                    mLoadListener.onLoadFinished();
                                }
                                if (BuildConfig.DEBUG) {
                                    Headers responseHeaders = response.headers();
                                    for (int i = 0, size = responseHeaders.size(); i < size; i++) {
                                        Log.d(ProductListAdapter.class.getSimpleName(), responseHeaders.name(i) + ": " + responseHeaders.value(i));
                                    }
                                    Log.d(ProductListAdapter.class.getSimpleName(), response.body().string());
                                }
                            } catch (IOException e) {
                                if (BuildConfig.DEBUG) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                }
            }
        };
        if (!mIsLoadingMore && mHasMoreProducts) {
            mIsLoadingMore = true;
            if (mLoadListener != null) {
                mLoadListener.onLoadStarted();
            }
            ProductUtils.getNextPageProducts(callback, mCurrentPage + 1);
        }
    }

    private void addToProductList(ArrayList<Product> products) {
        mProductList.addAll(products);
        notifyDataSetChanged();
        if (mLoadListener != null) {
            mLoadListener.onNextItemsLoaded(products.size());
            mLoadListener.onLoadFinished();
        }
    }

    public void clearAdapter() {
        mProductList.clear();
        notifyDataSetChanged();
    }

    /**
     * Sets a listener that can be used to know when results have been received.
     * @param loadListener What's listening?
     */
    public void setLoadListener(ProductListActivity loadListener) {
        mLoadListener = loadListener;
    }

    public ProductListActivity getLoadListener() {
        return mLoadListener;
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {

        private FrameLayout mParentLayout;
        private ImageView mIvProductImage;
        private TextView mTvName;
        private LinearLayout mLlRatingSection;
        private RatingStarsView mRsvRating;
        private TextView mTvNumRatings;
        private TextView mTvPrice;
        private TextView mTvStockStatus;

        public ProductViewHolder(View itemView) {
            super(itemView);
            mParentLayout = (FrameLayout) itemView.findViewById(R.id.product_list_item_parent_layout);
            mIvProductImage = (ImageView) itemView.findViewById(R.id.product_list_item_iv_product_image);
            mTvName = (TextView) itemView.findViewById(R.id.product_list_item_tv_name);
            mLlRatingSection = (LinearLayout) itemView.findViewById(R.id.product_list_item_ll_rating_layout);
            mRsvRating = (RatingStarsView) itemView.findViewById(R.id.product_list_item_rsv_rating);
            mTvNumRatings = (TextView) itemView.findViewById(R.id.product_list_item_tv_number_ratings);
            mTvPrice = (TextView) itemView.findViewById(R.id.product_list_item_tv_price);
            mTvStockStatus = (TextView) itemView.findViewById(R.id.product_list_item_tv_stock_status);
        }
    }

    public interface OnLoadListener {
        void onFirstItemsLoaded(int numLoaded);

        void onNextItemsLoaded(int numLoaded);

        void onFirstItemsFailure();

        void onNextItemsFailure();

        void onLoadStarted();

        void onLoadFinished();
    }
}
