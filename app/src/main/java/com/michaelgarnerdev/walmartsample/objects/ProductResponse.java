package com.michaelgarnerdev.walmartsample.objects;

import java.util.ArrayList;

/**
 * Created by mgarner on 4/12/2016.
 * Helps facilitate communication and retrieval of information from the API. OkHttp only allows one looksie into the response body string.
 * This helps store that response body so that the total number of products and the products themselves can both be retrieved.
 */
public class ProductResponse {

    private ArrayList<Product> mProducts;
    private int mTotalProducts = 0;

    public ProductResponse(){

    }

    public void setProducts(ArrayList<Product> products) {
        mProducts = products;
    }

    public void setTotalProducts(int totalProducts) {
        mTotalProducts = totalProducts;
    }

    public ArrayList<Product> getProducts() {
        return mProducts;
    }

    public int getTotalProducts() {
        return mTotalProducts;
    }
}
