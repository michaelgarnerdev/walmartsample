package com.michaelgarnerdev.walmartsample.objects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mgarner on 4/11/2016.
 * Object that models Products retrieved from the Walmart Test API. Implements Parcelable for use in intents.
 */
@SuppressWarnings("unused")
public class Product implements Parcelable {

    private static final String KEY_PRODUCT_ID = "productId";
    private static final String KEY_PRODUCT_NAME = "productName";
    private static final String KEY_PRODUCT_SHORT_DESCRIPTION = "shortDescription";
    private static final String KEY_PRODUCT_LONG_DESCRIPTION = "longDescription";
    private static final String KEY_PRODUCT_PRICE = "price";
    private static final String KEY_PRODUCT_IMAGE = "productImage";
    private static final String KEY_PRODUCT_REVIEW_RATING = "reviewRating";
    private static final String KEY_PRODUCT_REVIEW_COUNT = "reviewCount";
    private static final String KEY_PRODUCT_IN_STOCK = "inStock";

    @SerializedName(KEY_PRODUCT_ID)
    private String mId;
    @SerializedName(KEY_PRODUCT_NAME)
    private String mName;
    @SerializedName(KEY_PRODUCT_PRICE)
    private String mPrice;
    @SerializedName(KEY_PRODUCT_IMAGE)
    private String mImageUrl;
    @SerializedName(KEY_PRODUCT_IN_STOCK)
    private boolean mStockStatus;
    @SerializedName(KEY_PRODUCT_REVIEW_RATING)
    private double mRating;
    @SerializedName(KEY_PRODUCT_REVIEW_COUNT)
    private int mNumRatings;
    @SerializedName(KEY_PRODUCT_SHORT_DESCRIPTION)
    private String mShortDescription;
    @SerializedName(KEY_PRODUCT_LONG_DESCRIPTION)
    private String mLongDescription;

    /**
     * Default constructor.
     */
    public Product() {

    }

    /**
     * Creates a product with all values set.
     * @param id The id of the product.
     * @param name The name of the product.
     * @param price The price of the product.
     * @param imageUrl The url of the image of the product.
     * @param shortDescription The short description of the product.
     * @param longDescription The long description of the product.
     * @param averageRating The average rating of the product.
     * @param numberRatings The total number of ratings the product has.
     * @param stockStatus Whether or not the product is in stock.
     */
    public Product(String id, String name, String price, String imageUrl, String shortDescription, String longDescription, double averageRating, int numberRatings, boolean stockStatus) {
        mId = id;
        mName = name;
        mPrice = price;
        mImageUrl = imageUrl;
        mShortDescription = shortDescription;
        mLongDescription = longDescription;
        mRating = averageRating;
        mNumRatings = numberRatings;
        mStockStatus = stockStatus;
    }

    protected Product(Parcel in) {
        mId = in.readString();
        mName = in.readString();
        mPrice = in.readString();
        mImageUrl = in.readString();
        mStockStatus = in.readByte() != 0;
        mRating = in.readDouble();
        mNumRatings = in.readInt();
        mShortDescription = in.readString();
        mLongDescription = in.readString();
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPrice() {
        return mPrice;
    }

    public void setPrice(String price) {
        mPrice = price;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public boolean getStockStatus() {
        return mStockStatus;
    }

    public boolean isStockStatus() {
        return mStockStatus;
    }

    public void setStockStatus(boolean stockStatus) {
        mStockStatus = stockStatus;
    }

    public double getRating() {
        return mRating;
    }

    public void setRating(double rating) {
        mRating = rating;
    }

    public int getNumRatings() {
        return mNumRatings;
    }

    public void setNumRatings(int numRatings) {
        mNumRatings = numRatings;
    }

    public String getShortDescription() {
        return mShortDescription;
    }

    public void setShortDescription(String shortDescription) {
        mShortDescription = shortDescription;
    }

    public String getLongDescription() {
        return mLongDescription;
    }

    public void setLongDescription(String longDescription) {
        mLongDescription = longDescription;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeString(mName);
        dest.writeString(mPrice);
        dest.writeString(mImageUrl);
        dest.writeByte((byte) (mStockStatus ? 1 : 0));
        dest.writeDouble(mRating);
        dest.writeInt(mNumRatings);
        dest.writeString(mShortDescription);
        dest.writeString(mLongDescription);
    }
}
